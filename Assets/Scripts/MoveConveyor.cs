﻿using UnityEngine;
using System.Collections;

public class MoveConveyor : MonoBehaviour
{
    public float speed;
    GameObject target;
    // Use this for initialization
    Rigidbody body;
    public bool shouldMove;
    /// <summary>
    /// Is this gameobject being dragged by the player?
    /// </summary>
    public bool isDragged;
    void Start()
    {
        shouldMove = true;
        target = GameObject.FindGameObjectWithTag("Target");
        body = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "Belt")
        {
            if (shouldMove)
            {
                Vector3 direction = (target.transform.position - transform.position).normalized;
                body.MovePosition(transform.position + direction * speed * GameManager.Instance.globalSpeed * Time.deltaTime);
            }
        }
    }
    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Belt")
            GameManager.Instance.objsOnBelt.Remove(gameObject);
        // GameManager.Instance.conveyorObjectCount--;
    }
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Belt")
        {
            for (int i = 0; i < GameManager.Instance.objsOnBelt.Count; i++)
            {
                if (gameObject.GetInstanceID() == GameManager.Instance.objsOnBelt[i].GetInstanceID())
                {
                    return;
                }
            }
            GameManager.Instance.objsOnBelt.Add(gameObject);
        }
        // GameManager.Instance.conveyorObjectCount++;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish")
        {
            GameManager.Instance.DestroyToyThreat(this.gameObject);
            if (this.tag == "Toy")
                GameManager.Instance.updateScore(1);
            else
                GameManager.Instance.updateScore(-4);
        }
        if (other.tag == "StopMoving" && shouldMove)
        {
            if (isDragged)
                Bezier3D.SetActive(false);
            shouldMove = false;
            if (this.tag == "Threat")
                GameManager.Instance.updateScore(1);
        }
        if (other.tag == "CrateEmptyZone")
        {
            GameManager.Instance.DestroyToyThreat(this.gameObject);
            if (this.tag == "Threat")
                GameManager.Instance.updateScore(1);
        }
    }
}
