﻿using UnityEngine;
using System.Collections;

public class MoveWithMouse : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
            var mainCamera = FindCamera();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin, mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
            {
                Bezier3D.start = gameObject.transform.position;
                gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, new Vector3(hit.point.x, gameObject.transform.position.y, hit.point.z),Time.deltaTime*20);
            }
        
    }
    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
}
