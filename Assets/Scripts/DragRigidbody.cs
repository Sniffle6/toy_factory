using System;
using System.Collections;
using UnityEngine;

public class DragRigidbody : MonoBehaviour
{
    const float k_Spring = 200f;
    const float k_Damper = 20;
    const float k_Drag = 1;
    const float k_AngularDrag = 0.5f;
    const float k_Distance = 0.1f;
    const bool k_AttachToCenterOfMass = true;
   public bool isMovingObj;
    private SpringJoint m_SpringJoint;


    private void Update()
    {
        // Make sure the user pressed the mouse down
        if (!Input.GetMouseButton(0))
        {
            return;
        }
        if (isMovingObj)
        {
            return;
        }
        var mainCamera = FindCamera();

        // We need to actually hit an object
        RaycastHit hit = new RaycastHit();
        if (
            !Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin,
                             mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100,
                             Physics.DefaultRaycastLayers))
        {
            return;
        }
        if (hit.collider.GetComponent<MoveConveyor>())
        {
            if (!hit.collider.GetComponent<MoveConveyor>().shouldMove)
            {
                return;
            }
        }
        // We need to hit a rigidbody that is not kinematic
        if (!hit.rigidbody || hit.rigidbody.isKinematic)
        {
            return;
        }
        hit.rigidbody.MovePosition(hit.transform.position + (Vector3.up));
        if (!m_SpringJoint)
        {
            var go = new GameObject("Rigidbody dragger");
            Rigidbody body = go.AddComponent<Rigidbody>();
            m_SpringJoint = go.AddComponent<SpringJoint>();
            body.isKinematic = true;
        }

        m_SpringJoint.transform.position = hit.point;
        m_SpringJoint.anchor = Vector3.zero;

        m_SpringJoint.spring = k_Spring;
        m_SpringJoint.damper = k_Damper;
        m_SpringJoint.maxDistance = k_Distance;
        m_SpringJoint.connectedBody = hit.rigidbody;

        StartCoroutine("DragObject", hit.distance);
    }


    private IEnumerator DragObject(float distance)
    {
        var oldDrag = m_SpringJoint.connectedBody.drag;
        var oldAngularDrag = m_SpringJoint.connectedBody.angularDrag;
        m_SpringJoint.connectedBody.drag = k_Drag;
        m_SpringJoint.connectedBody.angularDrag = k_AngularDrag;
        var mainCamera = FindCamera();
        while (Input.GetMouseButton(0))
        {
            if (m_SpringJoint.connectedBody && m_SpringJoint.connectedBody.GetComponent<MoveConveyor>().shouldMove)
            {
                var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                m_SpringJoint.transform.position = ray.GetPoint(distance);
                Bezier3D.end = m_SpringJoint.connectedBody.transform.position;
                if (!isMovingObj)
                {
                    Bezier3D.SetActive(true);
                    isMovingObj = true;
                    m_SpringJoint.connectedBody.gameObject.GetComponent<MoveConveyor>().isDragged = true;
                }
            }
            yield return null;

        }
        if (isMovingObj)
        {
            isMovingObj = false;
            Bezier3D.SetActive(false);
            if(m_SpringJoint.connectedBody)
            m_SpringJoint.connectedBody.gameObject.GetComponent<MoveConveyor>().isDragged = false;
        }
        if (m_SpringJoint.connectedBody)
        {
            m_SpringJoint.connectedBody.drag = oldDrag;
            m_SpringJoint.connectedBody.angularDrag = oldAngularDrag;
            m_SpringJoint.connectedBody = null;
        }
    }


    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
}
