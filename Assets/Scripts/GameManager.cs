﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public bool debug;

    public GameObject[] toys;
    public GameObject[] threats;
    public List<GameObject> objsOnBelt = new List<GameObject>();
    public BoxCollider spawnSpot;
   // public int conveyorObjectCount;
    public bool isSpawning;
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }
    public float globalSpeed;
    public bool corutinePaused;
    bool spawnCorutine;
    public Vector2 spawnMinMax;
    public int maxEnemies;
    public Text scoreText;
    public int score;
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Use this for initialization
    void Start()
    {
        spawnCorutine = true;
        //conveyorObjectCount = 0;
        StartCoroutine(spawn());
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void SpawnToyThreat()
    {
        //conveyorObjectCount++;
        float random = Random.value;
        if (random >= 0.5f)
        {
            SpawnToy();
        }
        else
        {
            SpawnThreat();
        }
        if (objsOnBelt.Count >= maxEnemies)
        {
            corutinePaused = true;
            isSpawning = false;
            print("Corutine pasued. Will not spawn next loop!");
        }
    }
    public void updateScore(int value)
    {
        score+=value;
        scoreText.text = score.ToString();
    }
    public void DestroyToyThreat(GameObject obj)
    {
        Destroy(obj);
        if (!isSpawning && objsOnBelt.Count < maxEnemies)
        {
            isSpawning = true;
            corutinePaused = false;
            if (debug) print("Corutine Resumed");
        }
    }
    public void adjustHP(HealthManager hm, float value)
    {
        hm.health += value;
        if (hm.health <= 0)
        {
           // conveyorObjectCount--;
            DestroyToyThreat(hm.gameObject);
            if(hm.tag == "Threat")
            {
                updateScore(2);
            }
        }
    }
    IEnumerator spawn()
    {
        if(debug) print("Started an Enumerator");

        while (spawnCorutine)
        {
            if (corutinePaused)
            {
                if (debug) print("Corutine currentley pasued");
                yield return new WaitUntil(() => corutinePaused == false);
                yield return new WaitForSeconds(Random.Range(spawnMinMax.x, spawnMinMax.y));
            }
            else
            {
                if (objsOnBelt.Count >= maxEnemies)
                {
                    Debug.LogWarning("Corutine paused without corutinePaused being true!");
                    yield return new WaitUntil(() => objsOnBelt.Count < maxEnemies);
                }
            }
            if (objsOnBelt.Count < maxEnemies)
            {
                if (debug) print("In While Loop");
                isSpawning = true;
                SpawnToyThreat();
                yield return new WaitForSeconds(Random.Range(spawnMinMax.x, spawnMinMax.y));
            }
        }
        Debug.LogWarning("Spawn corutine ended. Was this intentional?");
    }
    Vector3 spawnPos()
    {
        Vector3 spawnLoc = spawnSpot.center;
        spawnLoc.x = Random.Range(-spawnSpot.size.x/2, spawnSpot.size.x/2);
        spawnLoc.y = Random.Range(-spawnSpot.size.y/2, spawnSpot.size.y/2);
        spawnLoc.z = Random.Range(-spawnSpot.size.z/2, spawnSpot.size.z/2);
        return spawnLoc;
    }
    private void SpawnToy()
    {
        int ran = Random.Range(0, toys.Length);
        GameObject obj = Instantiate(toys[ran], spawnSpot.transform) as GameObject;
        obj.transform.localPosition = spawnPos();
        obj.transform.Rotate(Vector3.up, Random.Range(0, 360));
    }

    private void SpawnThreat()
    {
        int ran = Random.Range(0, threats.Length);
        GameObject obj = Instantiate(threats[ran], spawnSpot.transform) as GameObject;
        obj.transform.localPosition = spawnPos();
        obj.transform.Rotate(Vector3.up, Random.Range(0, 360));
    }
}
