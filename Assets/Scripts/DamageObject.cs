﻿using UnityEngine;
using System.Collections;

public class DamageObject : MonoBehaviour
{
    public float damage;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var mainCamera = FindCamera();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin, mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
            {
                if (hit.collider.tag == "Threat")
                {
                    GameManager.Instance.adjustHP(hit.collider.GetComponent<HealthManager>(), -damage);
                }
            }
        }
    }
    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }

}
