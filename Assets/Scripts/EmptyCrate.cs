﻿using UnityEngine;
using System.Collections;

public class EmptyCrate : MonoBehaviour
{
    GameObject emptyZone;
    Renderer rend;
    Material mat;
    Light lit;

    // Use this for initialization
    void Start()
    {
        emptyZone = gameObject.transform.FindChild("EmptyArea").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var mainCamera = FindCamera();
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition).origin, mainCamera.ScreenPointToRay(Input.mousePosition).direction, out hit, 100, Physics.DefaultRaycastLayers))
            {
                if (hit.collider.tag != "EmptyButton")
                    return;
                if (hit.collider.transform.parent.parent.name == this.name)
                {
                    rend = hit.collider.GetComponent<Renderer>();
                    mat = rend.material;
                    lit = hit.collider.GetComponent<Light>();
                    StartCoroutine(emptyCrate());
                }
            }
        }
    }
    private Camera FindCamera()
    {
        if (GetComponent<Camera>())
        {
            return GetComponent<Camera>();
        }

        return Camera.main;
    }
    IEnumerator emptyCrate()
    {
        float emission = 1;
        mat.EnableKeyword("_EMISSION");
        mat.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
        mat.SetColor("_EmissionColor", Color.red * Mathf.LinearToGammaSpace(emission));
        mat.color = Color.red;
        lit.color = Color.red;
        emptyZone.SetActive(true);
        yield return new WaitForSeconds(1);
        mat.SetColor("_EmissionColor", Color.green * Mathf.LinearToGammaSpace(emission));
        mat.color = Color.green;
        lit.color = Color.green;
        emptyZone.SetActive(false);
    }
}
