﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bezier3D : MonoBehaviour
{
    public static Vector3 start = new Vector3(0, 0, 0);
    public static Vector3 end = new Vector3(1, 1, 0);
    Vector3 handle1 = new Vector3(0, 1, 0);
    Vector3 handle2 = new Vector3(1, 0, 0);
    public int resolution = 12;
    public float thickness = 0.25f;
    float rangeModifier=1.25f;

    List<Vector3> vertList = new List<Vector3>();
    List<int> triList = new List<int>();
    List<Vector2> uvList = new List<Vector2>();
    Vector3 upNormal = new Vector3(0, 0, -1);
    Mesh m_Mesh;
    static MeshRenderer m_Render;
    static ParticleSystem ps;
    static bool psPlaying;
    public void Start()
    {
        Vector3 midPoint = (start + end) / 2;
        handle2 = new Vector3(Random.Range(midPoint.x - rangeModifier, midPoint.x + rangeModifier), Random.Range(midPoint.y - rangeModifier, midPoint.y + rangeModifier), Random.Range(midPoint.z - rangeModifier, midPoint.z + rangeModifier));
        handle1 = new Vector3(Random.Range(midPoint.x - rangeModifier, midPoint.x + rangeModifier), Random.Range(midPoint.y - rangeModifier, midPoint.y + rangeModifier), Random.Range(midPoint.z - rangeModifier, midPoint.z + rangeModifier));
        m_Mesh = GetComponent<MeshFilter>().mesh = CreateMesh();
        m_Render = GetComponent<MeshRenderer>();

        ps = GetComponent<ParticleSystem>();
        var sh = ps.shape;
        sh.enabled = true;
        sh.shapeType = ParticleSystemShapeType.Mesh;
        sh.mesh = m_Mesh;
        ps.Stop();
        SetActive(false);

    }
    void Update()
    {
        if (m_Render.enabled)
        {
            Material newMat = GetComponent<Renderer>().material;
            newMat.SetFloat("_StartSeed", Random.value * 1000);
            GetComponent<Renderer>().material = newMat;
            m_Mesh = updateMesh(m_Mesh);
        }
    }
    public static void SetActive(bool value)
    {
        if (m_Render.enabled != value)
            m_Render.enabled = value;
        if (value)
        {
            if (!psPlaying)
            {
                ps.Play();
                psPlaying = true;
            }
        }
        else
        {
            if (psPlaying)
            {
                ps.Stop();
                psPlaying = false;
            }
        }
    }
    //cacluates point coordinates on a quadratic curve
    public static Vector3 PointOnPath(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u, uu, uuu, tt, ttt;
        Vector3 p;

        u = 1 - t;
        uu = u * u;
        uuu = uu * u;

        tt = t * t;
        ttt = tt * t;

        p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
    public Mesh updateMesh(Mesh mesh)
    {
        Vector3 midPoint = (start + end) / 2;
        handle2 = new Vector3(Random.Range(midPoint.x - rangeModifier, midPoint.x + rangeModifier), Random.Range(midPoint.y - rangeModifier, midPoint.y + rangeModifier), Random.Range(midPoint.z - rangeModifier, midPoint.z + rangeModifier));
        handle1 = new Vector3(Random.Range(midPoint.x - rangeModifier, midPoint.x + rangeModifier), Random.Range(midPoint.y - rangeModifier, midPoint.y + rangeModifier), Random.Range(midPoint.z - rangeModifier, midPoint.z + rangeModifier));
        float scaling = 1;
        float width = thickness / 2f;
        List<Vector3> vertListNew = new List<Vector3>();
        List<int> triListNew = new List<int>();
        List<Vector2> uvListNew = new List<Vector2>();
        Vector3 upNormalNew = new Vector3(0, 0, -1);

        triListNew.AddRange(new int[] {
            2, 1, 0,    //start face
			0, 3, 2
        });

        for (int s = 0; s < resolution; s++)
        {
            float t = ((float)s) / resolution;
            float futureT = ((float)s + 1) / resolution;


            Vector3 segmentStart = PointOnPath(t, start, handle1, handle2, end);
            Vector3 segmentEnd = PointOnPath(futureT, start, handle1, handle2, end);

            Vector3 segmentDirection = segmentEnd - segmentStart;
            if (s == 0 || s == resolution - 1)
                segmentDirection = new Vector3(0, 1, 0);
            segmentDirection.Normalize();
            Vector3 segmentRight = Vector3.Cross(upNormalNew, segmentDirection);
            segmentRight *= width;
            Vector3 offset = segmentRight.normalized * (width / 2) * scaling;
            Vector3 br = segmentRight + upNormalNew * width + offset;
            Vector3 tr = segmentRight + upNormalNew * -width + offset;
            Vector3 bl = -segmentRight + upNormalNew * width + offset;
            Vector3 tl = -segmentRight + upNormalNew * -width + offset;

            int curTriIdx = vertListNew.Count;

            Vector3[] segmentVerts = new Vector3[]
            {
                segmentStart + br,
                segmentStart + bl,
                segmentStart + tl,
                segmentStart + tr,
            };
            vertListNew.AddRange(segmentVerts);

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 1)
            };
            uvListNew.AddRange(uvs);

            int[] segmentTriangles = new int[]
            {
                curTriIdx + 6, curTriIdx + 5, curTriIdx + 1, //left face
				curTriIdx + 1, curTriIdx + 2, curTriIdx + 6,
                curTriIdx + 7, curTriIdx + 3, curTriIdx + 0, //right face
				curTriIdx + 0, curTriIdx + 4, curTriIdx + 7,
                curTriIdx + 1, curTriIdx + 5, curTriIdx + 4, //top face
				curTriIdx + 4, curTriIdx + 0, curTriIdx + 1,
                curTriIdx + 3, curTriIdx + 7, curTriIdx + 6, //bottom face
				curTriIdx + 6, curTriIdx + 2, curTriIdx + 3
            };
            triListNew.AddRange(segmentTriangles);

            // final segment fenceposting: finish segment and add end face
            if (s == resolution - 1)
            {
                curTriIdx = vertListNew.Count;

                vertListNew.AddRange(new Vector3[] {
                    segmentEnd + br,
                    segmentEnd + bl,
                    segmentEnd + tl,
                    segmentEnd + tr
                });

                uvListNew.AddRange(new Vector2[] {
                        new Vector2(0, 0),
                        new Vector2(0, 1),
                        new Vector2(1, 1),
                        new Vector2(1, 1)
                    }
                );
                triListNew.AddRange(new int[] {
                    curTriIdx + 0, curTriIdx + 1, curTriIdx + 2, //end face
					curTriIdx + 2, curTriIdx + 3, curTriIdx + 0
                });
            }
        }

        // mesh.vertices = vertListNew.ToArray();
        mesh.SetVertices(vertListNew);
        // mesh.triangles = triListNew.ToArray();
        mesh.SetTriangles(triListNew, 0);
        //mesh.uv = uvListNew.ToArray();
        mesh.SetUVs(0, uvListNew);
        // mesh.RecalculateNormals();
        // mesh.RecalculateBounds();
        //mesh.Optimize();

        return mesh;
    }
    public Mesh CreateMesh()
    {
        Mesh mesh;

        mesh = new Mesh();

        float scaling = 1;
        float width = thickness / 2f;

        triList.AddRange(new int[] {
            2, 1, 0,    //start face
			0, 3, 2
        });

        for (int s = 0; s < resolution; s++)
        {
            float t = ((float)s) / resolution;
            float futureT = ((float)s + 1) / resolution;

            Vector3 segmentStart = PointOnPath(t, start, handle1, handle2, end);
            Vector3 segmentEnd = PointOnPath(futureT, start, handle1, handle2, end);

            Vector3 segmentDirection = segmentEnd - segmentStart;
            if (s == 0 || s == resolution - 1)
                segmentDirection = new Vector3(0, 1, 0);
            segmentDirection.Normalize();
            Vector3 segmentRight = Vector3.Cross(upNormal, segmentDirection);
            segmentRight *= width;
            Vector3 offset = segmentRight.normalized * (width / 2) * scaling;
            Vector3 br = segmentRight + upNormal * width + offset;
            Vector3 tr = segmentRight + upNormal * -width + offset;
            Vector3 bl = -segmentRight + upNormal * width + offset;
            Vector3 tl = -segmentRight + upNormal * -width + offset;

            int curTriIdx = vertList.Count;

            Vector3[] segmentVerts = new Vector3[]
            {
                segmentStart + br,
                segmentStart + bl,
                segmentStart + tl,
                segmentStart + tr,
            };
            vertList.AddRange(segmentVerts);

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 1)
            };
            uvList.AddRange(uvs);

            int[] segmentTriangles = new int[]
            {
                curTriIdx + 6, curTriIdx + 5, curTriIdx + 1, //left face
				curTriIdx + 1, curTriIdx + 2, curTriIdx + 6,
                curTriIdx + 7, curTriIdx + 3, curTriIdx + 0, //right face
				curTriIdx + 0, curTriIdx + 4, curTriIdx + 7,
                curTriIdx + 1, curTriIdx + 5, curTriIdx + 4, //top face
				curTriIdx + 4, curTriIdx + 0, curTriIdx + 1,
                curTriIdx + 3, curTriIdx + 7, curTriIdx + 6, //bottom face
				curTriIdx + 6, curTriIdx + 2, curTriIdx + 3
            };
            triList.AddRange(segmentTriangles);

            // final segment fenceposting: finish segment and add end face
            if (s == resolution - 1)
            {
                curTriIdx = vertList.Count;

                vertList.AddRange(new Vector3[] {
                    segmentEnd + br,
                    segmentEnd + bl,
                    segmentEnd + tl,
                    segmentEnd + tr
                });

                uvList.AddRange(new Vector2[] {
                        new Vector2(0, 0),
                        new Vector2(0, 1),
                        new Vector2(1, 1),
                        new Vector2(1, 1)
                    }
                );
                triList.AddRange(new int[] {
                    curTriIdx + 0, curTriIdx + 1, curTriIdx + 2, //end face
					curTriIdx + 2, curTriIdx + 3, curTriIdx + 0
                });
            }
        }

        mesh.vertices = vertList.ToArray();
        mesh.triangles = triList.ToArray();
        mesh.uv = uvList.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        ;

        return mesh;
    }
}